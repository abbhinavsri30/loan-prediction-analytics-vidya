# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 18:16:14 2017

@author: Abhinav
"""

#Loan data model

import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier

train_loc = r"train_imputed.csv"
test_loc = r"test_imputed.csv"
test_loanID = r"test.csv"
submit_loc = r"submit.csv"

train = pd.read_csv(train_loc)
test = pd.read_csv(test_loc)
test_loanID = pd.read_csv(test_loanID)

#Set the target variable.

y = train.Loan_Status

#Drop the unnecessary columns and prepare or data for our submission file.

train =  train.drop(['Loan_Status'], axis=1)
X = train.astype(np.float32)
submit = test_loanID['Loan_ID']

#Use Scikit-Learn's train_test_split function to create train and validation sets.

X_train, X_val, y_train, y_val = train_test_split(X, y, train_size=0.8)

#Create a function to run a gradient boosted classifier over our data. The reason we have to
#do this is due to the way Python uses parallelization on Windows.
#https://www.kaggle.com/c/malware-classification/forums/t/12802/
#anyone-getting-parallelizing-error-for-scikit-learn-based-models-in-python/66187#post66187

#Note numerous different values were used in the param_grid to hone in on the best paramater
#combinations. The param grid below is the final one I used.

def model(X_train, X_val, y_train, y_val):
    if __name__ == '__main__':
    
        param_grid = {'learning_rate': [0.03, 0.035],
                      'max_depth': [3, 4, 5],
                      'min_samples_leaf': [17, 18],
                      'max_features': [1.0, 0.95, 0.9],
                      'n_estimators': [100, 300, 500]
                      }

        estimator = GridSearchCV(estimator=GradientBoostingClassifier(),
                                 param_grid=param_grid,
                                 n_jobs=-1)

        estimator.fit(X_train, y_train)

        best_params = estimator.best_params_
                                 
        validation_accuracy = estimator.score(X_val, y_val)
        print('Validation accuracy: ', validation_accuracy)
        
        return best_params
    
    
#params that appeared most often after running the model ten times.
    
params = {'min_samples_leaf': 17, 'max_features': 0.95, 'max_depth': 3,
          'learning_rate': 0.03, 'n_estimators': 500}

#Fit model using our data and the best parameters found by GridSearchCV.

model = GradientBoostingClassifier(**params)
model.fit(X, y)

#Make predictions on the test set.

preds = model.predict(test)

#Create submission file.

preds = pd.Series(preds)
submit = pd.concat([submit, preds], names=['Loan_ID', 'Loan_Status'], axis=1)
submit.columns = ['Loan_ID', 'Loan_Status']

#Create CSV file for submission.

submit.to_csv('loan1.csv', index=False)